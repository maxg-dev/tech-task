import React, { useState } from 'react'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import styles from './SignUp.module.css'
import CompletionScreen from './CompletionScreen'
import MultiPageForm from './MultiPageForm'

const STATUS = Object.freeze({
    NOT_SUBMITTED: "not submitted",
    SUCCESSFULLY_SUBMITTED: "successfully submitted",
    UNSUCCESSFULLY_SUBMITTED: "unsuccessfully submitted"
})

export default function SignUp() {
    const [formStatus, setFormStatus] = useState(STATUS.NOT_SUBMITTED)
    const formik = useFormik({
        initialValues: {
            name: "",
            number: "",
            email: "",
            dob: "",
        },
        validationSchema: Yup.object({
            name: Yup.string()
                .matches(/^[^0-9]+$/, "Numbers are not allowed")
                .min(2, "Must be a minimum of 2 characters")
                .max(30, "Must be 30 characters or less")
                .required("Required"),
            number: Yup.string()
                .matches(/^\+?\d+$/, "Invalid phone number")
                .min(7, "Number is too short")
                .max(15, "Number is too long")
                .required("Required"),
            email: Yup.string()
                .email("Invalid email address")
                .required("Required"),
            dob: Yup.date()
                .required("Required")
        }),
        onSubmit: (values, { setSubmitting }) => {
            setSubmitting(true)
            const possibilities = [STATUS.SUCCESSFULLY_SUBMITTED, STATUS.UNSUCCESSFULLY_SUBMITTED]
            const index = Math.round(Math.random())
            setFormStatus(possibilities[index])
            setSubmitting(false)
        },
    })

    return <div className={styles.container}>
        {formStatus === STATUS.NOT_SUBMITTED ? <MultiPageForm formik={formik} styles={styles} />
            : <CompletionScreen formStatus={formStatus} />}
    </div>
}