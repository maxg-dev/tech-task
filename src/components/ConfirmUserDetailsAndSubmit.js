import React from 'react'
import { NavLink } from 'react-router-dom';
import NextButton from './NextButton';

export default function ConfirmUserDetailsAndSubmit({ formik, styles }) {

    const isValid = Object.keys(formik.touched).length === 4 && Object.keys(formik.errors).length === 0

    return <form className={styles.form} onSubmit={formik.handleSubmit}>
        <p>Please confirm your details before submitting them.</p>
        <ul className={styles.ul}>
            {Object.keys(formik.values).map(k => <li key={k}>{`${k}: ${formik.values[k]}`}</li>)}
        </ul>
        <NavLink className={styles.button} to="/step2">Previous</NavLink>
        <NextButton isSubmitType={true} text="Submit" disabled={!isValid} styles={styles} />
    </form>
}