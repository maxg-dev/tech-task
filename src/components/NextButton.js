import { NavLink } from 'react-router-dom';

export default function NextButton({to, text, disabled, styles, isSubmitType=false}) {

    let classes = `${styles.button} ${styles.next} `
    if (disabled) classes += styles.disabled

    return isSubmitType ? 
        <button className={classes} type="submit" disabled={disabled}>{text}</button> :
        <NavLink className={classes} to={disabled ? "#" : to}>{text}</NavLink>
}