import React from 'react'

// Added formik.handleBlur to select event so that inputs are "touched" immediately (instead of only after blur event).

export default function FormikInput({ name, label, type, formik, styles, ...rest }) {
    return <div className={styles.input}>
        <label htmlFor={name}>{label}</label>
        <input id={name}
            name={name}
            type={type}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            onSelect={formik.handleBlur}
            value={formik.values[name]}
            {...rest} />
        {formik.touched[name] && formik.errors[name] ? (
            <div className={styles.error}>{formik.errors[name]}</div>
        ) : null}
    </div>
}