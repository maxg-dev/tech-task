import React from 'react'
import FormikInput from './FormikInput'
import NextButton from './NextButton';

export default function NameAndNumber({ formik, styles }) {

    const hasErrors = "name" in formik.errors || "number" in formik.errors
    const isValid = "name" in formik.touched && "number" in formik.touched && !hasErrors

    return <form className={styles.form}>
        <FormikInput name="name" label="Name" type="text" formik={formik} styles={styles} />
        <FormikInput name="number" label="Phone number" type="text" formik={formik} styles={styles} />
        <NextButton to="/step2" disabled={!isValid} styles={styles} text="Next" />
    </form>
}