import React from 'react'
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom';
import NameAndNumber from './NameAndNumber'
import EmailAndDob from './EmailAndDob'
import ConfirmUserDetailsAndSubmit from './ConfirmUserDetailsAndSubmit'

export default function MultiPageForm({ formik, styles }) {
    return <Router>
        <Switch>
            <Redirect exact from="/" to="/step1" />
            <Route path="/step1">
                <NameAndNumber formik={formik} styles={styles} />
            </Route>
            <Route path="/step2">
                <EmailAndDob formik={formik} styles={styles} />
            </Route>
            <Route path="/step3">
                <ConfirmUserDetailsAndSubmit formik={formik} styles={styles} />
            </Route>
        </Switch>
    </Router>
}

