import React from 'react'

export default function CompletionScreen({ formStatus }) {

    return <>
        {formStatus === "successfully submitted" ?
            <p>You have successfully signed up, thank you.</p> :
            <p>There was a problem signing up. Please try again later.</p>}
        <a href="/">RETEST</a>
    </>
}