import React from 'react'
import FormikInput from './FormikInput'
import { NavLink } from 'react-router-dom';
import NextButton from './NextButton'

export default function EmailAndDob({ formik, styles }) {

    const hasErrors = "email" in formik.errors || "dob" in formik.errors
    const isValid = "email" in formik.touched && "dob" in formik.touched && !hasErrors

    return <form className={styles.form}>
        <FormikInput name="dob" label="Date of birth" type="date" formik={formik} styles={styles} />
        <FormikInput name="email" label="Email" type="email" formik={formik} styles={styles} />
        <NavLink className={styles.button} to="/step1">Previous</NavLink>
        <NextButton to="/step3" disabled={!isValid} styles={styles} text="Next" />
    </form>
}